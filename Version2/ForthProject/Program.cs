﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ForthProject
{
    class Program
    {
        public class DateTimeInstance
        {
            public DateTime DateTime;
            public double[][] Data;
        }
        static void Main(string[] args)
        {
            //Authenticate
            AuthenticationService.AuthenticationServiceSoapClient authenticationClient = new AuthenticationService.AuthenticationServiceSoapClient();
            var token = authenticationClient.AuthenticateCredentials(null, "halizadehir@gmail.com", "hossein");
            //READY DATA
            List<string> currencies = new List<string> { "USD", "EUR", "GBP", "JPY", "XAU", "AUD", "CHF", "CAD", "NZD", "DKK", "HKD", "XAG", "NOK", "SEK", "SGD", "HUF" };
            List<DateTimeInstance> data = new List<DateTimeInstance>();
            DateTime startDate = DateTime.Now.AddDays(-3);
            DateTime endDate = DateTime.Now;
            ChartingService.TimeFrame timeFrame = ChartingService.TimeFrame.FIVE_MINUTE;
            ChartingService.ChartingServiceSoapClient charClient = new ChartingService.ChartingServiceSoapClient();
            //
            for (int i = 0; i < currencies.Count; i++)
            {
                for (int j = 0; j < currencies.Count; j++)
                {
                    if (i == j)
                        continue;
                    var tempData = charClient.GetChartData(null, token.token, currencies[i] + "/" + currencies[j], timeFrame, startDate, endDate);
                    if (string.IsNullOrEmpty(tempData.Data))
                        continue;
                    var byDate = tempData.Data.Split('$');
                    foreach (var date in byDate)
                    {
                        var parts = date.Split('\\');
                        DateTime parsedDate = DateTime.Parse(parts[0]);
                        var selectedD = data.FirstOrDefault(d => d.DateTime == parsedDate);
                        if (selectedD == null)
                        {
                            selectedD = new DateTimeInstance()
                            {
                                DateTime = parsedDate,
                                Data = new double[currencies.Count][],
                            };
                            data.Add(selectedD);
                        }
                        if (selectedD.Data[i] == null)
                            selectedD.Data[i] = new double[currencies.Count];
                        if (selectedD.Data[j] == null)
                            selectedD.Data[j] = new double[currencies.Count];
                        selectedD.Data[i][j] = (double.Parse(parts[1]) + double.Parse(parts[2]) + double.Parse(parts[3]) + double.Parse(parts[4])) / 4;
                        selectedD.Data[j][i] = 1 / selectedD.Data[i][j];
                    }
                }
            }
        }
    }
}
