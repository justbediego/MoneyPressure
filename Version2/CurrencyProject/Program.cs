﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyProject
{
    class Program
    {
        public static List<DailyData> WholeData = new List<DailyData>();
        public static List<int> maps = new List<int>();
        static void Main(string[] args)
        {
            List<string> currencies = new List<string>();
            var files = Directory.GetFiles("D:\\Needs\\Others\\CurrencyData\\").ToList();
            double count = 0;
            foreach (string file in files)
            {
                count++;
                Console.Title = (count / files.Count).ToString();
                string name = file.Split('\\').Last();
                if (name.Length == 10)
                {
                    string cur1 = name.Substring(0, 3);
                    string cur2 = name.Substring(3, 3);
                    int index1 = currencies.IndexOf(cur1);
                    if (index1 == -1)
                    {
                        index1 = currencies.Count;
                        currencies.Add(cur1);
                    }
                    int index2 = currencies.IndexOf(cur2);
                    if (index2 == -1)
                    {
                        index2 = currencies.Count;
                        currencies.Add(cur2);
                    }
                    var lines = File.ReadAllLines(file).Skip(1).ToList();
                    lines.ForEach(line =>
                    {
                        var parts = line.Split(',');
                        DateTime date = DateTime.Parse(parts[0].Substring(0, 4) + "-" + parts[0].Substring(4, 2) + "-" + parts[0].Substring(6, 2));
                        int dateInNumber = int.Parse(parts[0]);
                        while (maps.Count <= dateInNumber)
                            maps.Add(-1);
                        if (maps[dateInNumber] == -1)
                        {
                            maps[dateInNumber] = WholeData.Count;
                            WholeData.Add(new DailyData()
                            {
                                Date = date,
                            });
                        }
                        DailyData dailyData = WholeData[maps[dateInNumber]];
                        double? previousValue = dailyData.GetValue(index1, index2);
                        double value = (double.Parse(parts[1]) + double.Parse(parts[4])) / 2;
                        if (previousValue.HasValue && (previousValue / value > 1.1 || value / previousValue > 1.1))
                        {
                            dailyData.SetValue(index1, index2, 0);
                            dailyData.SetValue(index2, index1, 0);
                        }
                        else
                        {
                            dailyData.SetValue(index1, index2, value);
                            dailyData.SetValue(index2, index1, 1 / value);
                        }
                    });
                }
            }
            //done
            StreamWriter streamWriter = new StreamWriter("d:\\output.txt");
            streamWriter.WriteLine(currencies.Count);
            currencies.ForEach(c =>
            {
                streamWriter.WriteLine(c);
            });
            streamWriter.WriteLine(WholeData.Count);
            WholeData.OrderBy(w => w.Date).ToList().ForEach(dd =>
            {
                streamWriter.WriteLine(dd.Date.ToString("yyyy-MM-dd"));
                for (int i = 0; i < currencies.Count; i++)
                {
                    for (int j = 0; j < currencies.Count; j++)
                    {
                        var c = dd.GetValue(i, j);
                        if (c == null)
                            streamWriter.WriteLine("0");
                        else
                            streamWriter.WriteLine(c.Value);
                    }
                }
            });
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
