﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyProject
{
    public class DailyData
    {

        public void SetValue(int i, int j, double value)
        {
            while (Matrix.Count <= i)
                Matrix.Add(new List<double?>());
            while (Matrix[i].Count <= j)
                Matrix[i].Add(null);
            Matrix[i][j] = value;
        }
        public double? GetValue(int i, int j)
        {
            if (Matrix.Count <= i)
                return null;
            if (Matrix[i].Count <= j)
                return null;
            return Matrix[i][j];
        }
        public List<List<double?>> Matrix = new List<List<double?>>();
        public DateTime Date;
    }
}
