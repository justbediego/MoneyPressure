clc;
clear;
load('FinalData.mat');
%cny-usd
index1=13;
index2=39;
previousAnalyze=30;
availableIndexes=1:size(FileData.Matrix,1);
availableIndexes=availableIndexes(FileData.Matrix(:,index1,index2)~=0);
dates=FileData.Dates(availableIndexes);
currencyChanges=FileData.Matrix(availableIndexes,index1,index2);
matrix=FileData.Matrix(availableIndexes,:,:);
dailyPressure=[];
sumPressure=[];
myMoney=0;
for i=previousAnalyze+1:length(availableIndexes)
    dailyPressure(i)=OnePressure2way(index1,index2,squeeze(matrix(i,:,:)))/currencyChanges(i);
    sumPressure(i)= sum(dailyPressure(i-previousAnalyze:i)-1);
    
end
hold on;
plot((currencyChanges/mean(currencyChanges))-1,'k');
plot(dailyPressure-1,'b');
plot(sumPressure,'r');

a=currencyChanges(end-100:end);
d=sumPressure(end-100:end);
money1=1;
money2=1;
history=[];
for i=1:100
    if(d(i)<0)
        percent=d(i)*-1*10;
        if(percent>1)
            percent=1;
        end
        money1ToSell=money1*percent;
        money2=money2+money1ToSell*a(i);
        money1=money1-money1ToSell;
        history(i)=percent*-1;
    else
        percent=d(i)*10;
        if(percent>1)
            percent=1;
        end
        money2ToSell=money2*percent;
        money1=money1+money2ToSell/a(i);
        money2=money2-money1ToSell;
        history(i)=percent;
    end
end
figure();
plot(history)
hold on;
plot (a/mean(a)-1,'r')



