function p=Pressure2way(matrix)
    len=length(matrix);
    p=zeros(len,len);
    for i=1:len
        for j=i+1:len
            p(i,j)=OnePressure2way(i,j,matrix);
        end
    end
end