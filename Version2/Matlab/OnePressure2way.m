function p=Pressure2way(i,j,matrix)
    len=length(matrix);
    p=0;
    count=0;
    for k=1:len
        if(k==i || k==j)
            continue;
        end
        v=matrix(i,k)*matrix(k,j);  
        if(v==0)
            continue;
        end
        count=count+1;
        p=p+v;
    end
    if(count~=0)
        p=p/count;
    end
end