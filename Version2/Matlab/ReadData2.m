function data=ReadData2()
    data=struct('Currencies',[],'Dates',[],'Matrix',[]);
    fid = fopen('D:/output.txt');
    nCurrencies = str2num(fgetl(fid));
    for i=1:nCurrencies
        c = fgetl(fid);
        data.Currencies=[data.Currencies {c}];
    end
    nDates = str2num(fgetl(fid));
    data.Matrix=zeros(nDates,nCurrencies,nCurrencies);
    for d=1:nDates
        disp(d/nDates);
        date = fgetl(fid);
        data.Dates=[data.Dates {date}];
        for i=1:nCurrencies
            for j=1:nCurrencies
                value=str2double(fgetl(fid));
                data.Matrix(d,i,j)=value;
            end
        end
    end
end