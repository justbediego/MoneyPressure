﻿using Core.Business;
using Core.DataAccess;
using Core.DataAccess.Repository;
using Core.DataAccess.UnitOfWork;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyze
{
    class Program
    {
        public class Money
        {
            public int CurrencyID;
            public double Amount;
        }
        static void Main(string[] args)
        {
            MoneyPressureContext dbContext = new MoneyPressureContext();
            UnitOfWork unitOfWork = new UnitOfWork(dbContext);
            RateRepository rateRepository = new RateRepository(dbContext);
            CurrencyRepository currencyRepository = new CurrencyRepository(dbContext);
            MainBusiness mainBusiness = new MainBusiness(unitOfWork, rateRepository, currencyRepository);
            List<Currency> currencies = mainBusiness.GetAllCurrencies();
            List<Money> money = currencies.Select(c => new Money() { CurrencyID = c.ID, Amount = 1 }).ToList();
            List<DateTime> dates = currencies.Select(c => c.Rates).SelectMany(r => r).Select(r => r.Date).Distinct().OrderBy(d => d.Date).ToList();
            for (int i = 2; i < dates.Count; i++)
            {
                var thisDate = dates[i];
                for (int j = 0; j < currencies.Count; j++)
                {
                    var thisDateRate = currencies[j].MainRates.Where(r => r.Date == thisDate).FirstOrDefault();
                    if (thisDateRate!=null)
                    {
                        var yesterdayRate = currencies[j].MainRates.Where(r => r.Date < thisDate).OrderByDescending(r => r.Date).First();
                        var dayBeforeRate = currencies[j].MainRates.Where(r => r.Date < yesterdayRate.Date).OrderByDescending(r => r.Date).First();

                        var todayPressure = thisDateRate.Pressure;
                        var yesterdayPressure = yesterdayRate.Pressure;
                        var dayBeforePressure = dayBeforeRate.Pressure;
                        if (yesterdayPressure > 0 && yesterdayPressure > todayPressure && yesterdayPressure > dayBeforePressure)
                        {
                            //max-gharare ziad beshe
                            var mainMoney = money.Where(m => m.CurrencyID == yesterdayRate.MainCurrency_ID).First();
                            var baseMoney = money.Where(m => m.CurrencyID == yesterdayRate.BaseCurrency_ID).First();

                            mainMoney.Amount += baseMoney.Amount / yesterdayRate.Value;
                            baseMoney.Amount = 0;
                        }
                        if (yesterdayPressure < 0 && yesterdayPressure < todayPressure && yesterdayPressure < dayBeforePressure)
                        {
                            //min-gharare kam beshe
                            var mainMoney = money.Where(m => m.CurrencyID == yesterdayRate.MainCurrency_ID).First();
                            var baseMoney = money.Where(m => m.CurrencyID == yesterdayRate.BaseCurrency_ID).First();

                            baseMoney.Amount += mainMoney.Amount * yesterdayRate.Value;
                            mainMoney.Amount = 0;
                        }

                    }
                }
            }
        }
    }
}
