﻿using Core.Business;
using Core.DataAccess;
using Core.DataAccess.Repository;
using Core.DataAccess.UnitOfWork;
using Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Eranico
{
    class Program
    {
        public class Series
        {
            public List<double> data=new List<double>();
        }
        public class CurrencyRate
        {
            public string MainCurrency;
            public string BaseCurrency;
            public double Value;
            public DateTime Date;
        }
        public static List<CurrencyRate> GetRates(string url, string mainCurrency, string baseCurrency)
        {
            HttpClient client = new HttpClient();
            string result = client.GetStringAsync(url).Result;
            string categories = result.Substring(result.IndexOf("categories:") + 12);
            categories = categories.Substring(0, categories.IndexOf(']') + 1);
            List<DateTime> Dates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(categories).Select(s => PersianDate.ConvertDate.ToEn(s)).ToList();
            string series = result.Substring(result.IndexOf("series:") + 8);
            int count = 0;
            int index = 0;
            while (true)
            {
                if (series[index] == '[')
                    count++;
                if (series[index] == ']')
                {
                    count--;
                    if (count == 0)
                        break;
                }
                index++;
            }
            series = series.Substring(0, index + 1);
            List<Series> SeriesData = JsonConvert.DeserializeObject<List<Series>>(series);
            var Values = SeriesData.Last().data;
            List<CurrencyRate> finalResult = new List<CurrencyRate>();
            for(int i = 0; i < Dates.Count; i++)
            {
                finalResult.Add(new CurrencyRate()
                {
                    BaseCurrency=baseCurrency,
                    MainCurrency=mainCurrency,
                    Date=Dates[i],
                    Value=Values[i],
                });
            }
            return finalResult;
        }
        static void Main(string[] args)
        {
            MoneyPressureContext dbContext = new MoneyPressureContext();
            UnitOfWork unitOfWork = new UnitOfWork(dbContext);
            RateRepository rateRepository=new RateRepository(dbContext);
            CurrencyRepository currencyRepository=new CurrencyRepository(dbContext);
            MainBusiness mainBusiness = new MainBusiness(unitOfWork, rateRepository, currencyRepository);
            var GoldByIRR = GetRates("http://www.eranico.com/fa/coin/66/1/2/0/3/67/28/0/0/0/0", "GOLD", "IRR");
            var GoldByUSD = GetRates("http://www.eranico.com/fa/coin/66/1/31/0/3/50/47/0/0/0/0", "GOLD", "USD").Select(cr => new CurrencyRate()
            {
                BaseCurrency=cr.BaseCurrency,
                MainCurrency=cr.MainCurrency,
                Date=cr.Date,
                Value = cr.Value / 28.349523125,
            }).ToList();
            var USDByEUR = GetRates("http://www.eranico.com/fa/currency/24/1/1912/0/3/32/47/0/0/0/0", "USD", "EUR");
            var EURByIRR = GetRates("http://www.eranico.com/fa/currency/24/1/5673/0/3/32/28/0/0/0/0", "EUR", "IRR");
            var USDByIRR = GetRates("http://www.eranico.com/fa/currency/20/1/5673/0/3/29/28/0/0/0/0", "USD", "IRR");
            //
            //GoldByIRR.ForEach(cr =>
            //{
            //    mainBusiness.InsertRate(new Rate()
            //    {
            //        BaseCurrency = new Currency()
            //        {
            //            Name=cr.BaseCurrency,
            //        },
            //        MainCurrency = new Currency()
            //        {
            //            Name=cr.MainCurrency,
            //        },
            //        Value=cr.Value,
            //        Date=cr.Date,
            //    });
            //});
            //GoldByUSD.ForEach(cr =>
            //{
            //    mainBusiness.InsertRate(new Rate()
            //    {
            //        BaseCurrency = new Currency()
            //        {
            //            Name = cr.BaseCurrency,
            //        },
            //        MainCurrency = new Currency()
            //        {
            //            Name = cr.MainCurrency,
            //        },
            //        Value = cr.Value,
            //        Date = cr.Date,
            //    });
            //});
            USDByEUR.ForEach(cr =>
            {
                mainBusiness.InsertRate(new Rate()
                {
                    BaseCurrency = new Currency()
                    {
                        Name = cr.BaseCurrency,
                    },
                    MainCurrency = new Currency()
                    {
                        Name = cr.MainCurrency,
                    },
                    Value = cr.Value,
                    Date = cr.Date,
                });
            });
            EURByIRR.ForEach(cr =>
            {
                mainBusiness.InsertRate(new Rate()
                {
                    BaseCurrency = new Currency()
                    {
                        Name = cr.BaseCurrency,
                    },
                    MainCurrency = new Currency()
                    {
                        Name = cr.MainCurrency,
                    },
                    Value = cr.Value,
                    Date = cr.Date,
                });
            });
            USDByIRR.ForEach(cr =>
            {
                mainBusiness.InsertRate(new Rate()
                {
                    BaseCurrency = new Currency()
                    {
                        Name = cr.BaseCurrency,
                    },
                    MainCurrency = new Currency()
                    {
                        Name = cr.MainCurrency,
                    },
                    Value = cr.Value,
                    Date = cr.Date,
                });
            });
        }
    }
}
