﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class Currency : Base
    {
        public string Name { get; set; }
        [ForeignKey("MainCurrency_ID")]
        public virtual List<Rate> MainRates { get; set; }
        [ForeignKey("BaseCurrency_ID")]
        public virtual List<Rate> BaseRates { get; set; }
        [NotMapped]
        public List<Rate> Rates
        {
            get
            {
                var result = MainRates.Select(r => new Rate()
                {
                    BaseCurrency = r.BaseCurrency,
                    MainCurrency = r.MainCurrency,
                    Date = r.Date,
                    Value = r.Value,
                    ID = r.ID,
                    DateCreated = r.DateCreated,
                }).ToList();
                result.AddRange(BaseRates.Select(r => new Rate()
                {
                    BaseCurrency = r.MainCurrency,
                    MainCurrency = r.BaseCurrency,
                    Date = r.Date,
                    Value = 1 / r.Value,
                    ID = r.ID,
                    DateCreated = r.DateCreated,
                }));
                return result;
            }
        }
        public Currency()
        {
            MainRates = new List<Rate>();
            BaseRates = new List<Rate>();
        }
    }
}
