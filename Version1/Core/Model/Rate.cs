﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class Rate : Base
    {
        public int? MainCurrency_ID { get; set; }
        [ForeignKey("MainCurrency_ID")]
        public virtual Currency MainCurrency { get; set; }
        public int? BaseCurrency_ID { get; set; }
        [ForeignKey("BaseCurrency_ID")]
        public virtual Currency BaseCurrency { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        [NotMapped]
        public double Pressure
        {
            get
            {
                List<double> otherRates = new List<double>();
                //main->main->base->main->base
                MainCurrency.MainRates.Where(r => r.Date == Date && r.BaseCurrency.MainRates.Any(r2 => r2.Date == Date && r2.BaseCurrency_ID == BaseCurrency.ID)).ToList().ForEach(r =>
                {
                    var rate2 = r.BaseCurrency.MainRates.Where(r2 => r2.Date == Date && r2.BaseCurrency_ID == BaseCurrency.ID).First();
                    otherRates.Add(r.Value * rate2.Value);
                    //done
                });
                //main->main->base->base->main
                MainCurrency.MainRates.Where(r => r.Date == Date && r.BaseCurrency.BaseRates.Any(r2 => r2.Date==Date && r2.MainCurrency_ID == BaseCurrency.ID)).ToList().ForEach(r =>
                {
                    var rate2 = r.BaseCurrency.BaseRates.Where(r2 => r2.Date == Date && r2.MainCurrency_ID == BaseCurrency.ID).First();
                    otherRates.Add(r.Value * (1/rate2.Value));
                    //done
                });
                //main->base->main->main->base
                MainCurrency.BaseRates.Where(r => r.Date == Date && r.MainCurrency.MainRates.Any(r2 => r2.Date == Date && r2.BaseCurrency_ID == BaseCurrency.ID)).ToList().ForEach(r =>
                {
                    var rate2 = r.MainCurrency.MainRates.Where(r2 => r2.Date == Date && r2.BaseCurrency_ID == BaseCurrency.ID).First();
                    otherRates.Add((1/r.Value) * rate2.Value);
                    //done
                });
                //main->base->main->base->main
                MainCurrency.BaseRates.Where(r => r.Date == Date && r.MainCurrency.BaseRates.Any(r2 => r2.Date == Date && r2.MainCurrency_ID == BaseCurrency.ID)).ToList().ForEach(r =>
                {
                    var rate2 = r.MainCurrency.BaseRates.Where(r2 => r2.Date == Date && r2.MainCurrency_ID == BaseCurrency.ID).First();
                    otherRates.Add((1/r.Value) * (1/rate2.Value));
                });
                double average = otherRates.Sum() / otherRates.Count;
                return average - Value;
            }
        }
    }
}
