﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class Base
    {
        [Key]
        public int ID { get; set; }
        public DateTime DateCreated { get; set; }
        public Base()
        {
            DateCreated = DateTime.Now;
        }
    }
}
