﻿using Core.DataAccess.Repository;
using Core.DataAccess.UnitOfWork;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Business
{
    public class MainBusiness
    {
        private UnitOfWork unitOfWork;
        private RateRepository rateRepository;
        private CurrencyRepository currencyRepository;
        public MainBusiness(UnitOfWork unitOfWork, RateRepository rateRepository, CurrencyRepository currencyRepository)
        {
            this.unitOfWork = unitOfWork;
            this.currencyRepository = currencyRepository;
            this.rateRepository = rateRepository;

        }
        public void InsertRate(Rate rate)
        {
            int count;
            Currency mainCurrency = currencyRepository.GetAll(out count, c => c.Name == rate.MainCurrency.Name).FirstOrDefault();
            if (mainCurrency == null)
            {
                mainCurrency = new Currency()
                {
                    Name = rate.MainCurrency.Name,
                };
                currencyRepository.Insert(mainCurrency);
            }
            Currency baseCurrency = currencyRepository.GetAll(out count, c => c.Name == rate.BaseCurrency.Name).FirstOrDefault();
            if (baseCurrency == null)
            {
                baseCurrency = new Currency()
                {
                    Name = rate.BaseCurrency.Name,
                };
                currencyRepository.Insert(baseCurrency);
            }
            Rate newRate = mainCurrency.MainRates.Where(r => r.Date == rate.Date && r.BaseCurrency_ID == baseCurrency.ID).FirstOrDefault();
            if (newRate == null)
                newRate = baseCurrency.BaseRates.Where(r => r.Date == rate.Date && r.MainCurrency_ID == mainCurrency.ID).FirstOrDefault();
            if (newRate == null)
                newRate = new Rate()
                {
                    BaseCurrency = baseCurrency,
                    MainCurrency = mainCurrency,
                    Date = rate.Date,
                };
            newRate.Value = rate.Value;
            mainCurrency.MainRates.Add(newRate);
            unitOfWork.Commit();
        }
        public List<Rate> GetRates(string currency1, string currency2){
            int count;
            return rateRepository.GetAll(out count, r => (r.MainCurrency.Name == currency1 && r.BaseCurrency.Name == currency2) || (r.MainCurrency.Name == currency2 && r.BaseCurrency.Name == currency1));
        }
        public List<Currency> GetAllCurrencies()
        {
            int count;
            return currencyRepository.GetAll(out count);
        }
    }
}
