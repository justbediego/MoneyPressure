﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Core.DataAccess.UnitOfWork
{
    public interface IUnitOfWork : IDisposable, IEnlistmentNotification
    {
        void Commit();
    }
}
