﻿
using Core.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataAccess.UnitOfWork
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : Base
    {
        protected readonly DbContext context;
        protected readonly DbSet<TEntity> dataSet;
        public GenericRepository(DbContext context)
        {
            this.context = context;
            this.dataSet = context.Set<TEntity>();
        }
        public virtual void Insert(TEntity entity)
        {
            dataSet.Add(entity);
        }
        public virtual List<TEntity> GetAll(out int count, Expression<Func<TEntity, bool>> where = null, int? skip = null, int? take = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> source = (IQueryable<TEntity>)this.dataSet;
            //before count
            if (where != null)
                source = Queryable.Where<TEntity>(source, where);
            foreach (Expression<Func<TEntity, object>> path in includes)
                source = source.Include(path);
            count = source.Count();
            //after count
            if (skip.HasValue)
                source = source.Skip(skip.Value);
            if (take.HasValue)
                source = source.Take(take.Value);
            return source.ToList();
        }
        public virtual TEntity Get(Expression<Func<TEntity, bool>> where = null, params Expression<Func<TEntity, object>>[] includes)
        {
            var source = (IQueryable<TEntity>)this.dataSet;
            if (where != null)
                source = Queryable.Where<TEntity>(source, where);
            foreach (Expression<Func<TEntity, object>> path in includes)
                source = source.Include(path);
            return source.SingleOrDefault();
        }
        public virtual int Count(Expression<Func<TEntity, bool>> where = null)
        {
            IQueryable<TEntity> source = (IQueryable<TEntity>)this.dataSet;
            if (where != null)
                source = Queryable.Where<TEntity>(source, where);
            return source.Count();
        }
        public void Update(TEntity model)
        {
            if (context.Entry<TEntity>(model).State == EntityState.Detached)
                dataSet.Attach(model);
            context.Entry<TEntity>(model).State = EntityState.Modified;
        }
        public void Delete(int ID)
        {
            TEntity model = Get(ID);
            dataSet.Remove(model);
        }
        public TEntity Get(int ID, params Expression<Func<TEntity, object>>[] includes)
        {
            TEntity result = Get(e => e.ID == ID, includes);
            if (result == null)
                throw new Exception(typeof(TEntity).Name);
            return result;
        }
    }
}