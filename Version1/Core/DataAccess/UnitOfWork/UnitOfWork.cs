﻿using System;
using System.Data.Entity;
using System.Transactions;

namespace Core.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable, IEnlistmentNotification
    {
        protected readonly DbContext context;
        private bool disposed;

        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }

        public void Commit()
        {
            if (this.disposed)
                UnitOfWork.ThrowObjectDisposedException();
            this.context.ChangeTracker.DetectChanges();
            this.context.SaveChanges();
        }

        private static void ThrowObjectDisposedException()
        {
            throw new ObjectDisposedException("UnitOfWork");
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if (disposing)
                this.context.Dispose();
            this.disposed = true;
        }

        void IEnlistmentNotification.Commit(Enlistment enlistment)
        {
            ((IEnlistmentNotification)this.context).Commit(enlistment);
        }

        void IEnlistmentNotification.InDoubt(Enlistment enlistment)
        {
            ((IEnlistmentNotification)this.context).InDoubt(enlistment);
        }

        void IEnlistmentNotification.Prepare(PreparingEnlistment preparingEnlistment)
        {
            ((IEnlistmentNotification)this.context).Prepare(preparingEnlistment);
        }

        void IEnlistmentNotification.Rollback(Enlistment enlistment)
        {
            ((IEnlistmentNotification)this.context).Rollback(enlistment);
        }
    }
}