﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataAccess.UnitOfWork
{
    public class UnitOfWorkFactory
    {
        public static IUnitOfWork Create(DbContext context)
        {
            return (IUnitOfWork)new UnitOfWork(context);
        }

        public static IUnitOfWork Create(string nameOrConnectionString)
        {
            return (IUnitOfWork)new UnitOfWork(new DbContext(nameOrConnectionString));
        }
    }
}
