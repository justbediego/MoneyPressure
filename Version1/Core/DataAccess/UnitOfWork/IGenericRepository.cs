﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataAccess.UnitOfWork
{
    public interface IGenericRepository<TEntity> where TEntity : Base
      {
        void Insert(TEntity model);
        void Update(TEntity model);
        void Delete(int ID);
        List<TEntity> GetAll(out int count, Expression<Func<TEntity, bool>> where = null, int? skip = null, int? take = null, params Expression<Func<TEntity, object>>[] includes);
        TEntity Get(Expression<Func<TEntity, bool>> where = null, params Expression<Func<TEntity, object>>[] includes);
        TEntity Get(int ID, params Expression<Func<TEntity, object>>[] includes);
        int Count(Expression<Func<TEntity, bool>> where = null);
    }
}