﻿using Core.DataAccess.UnitOfWork;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataAccess.Repository
{
    public class CurrencyRepository : GenericRepository<Currency>
    {
        public CurrencyRepository(MoneyPressureContext dbContext)
            : base(dbContext)
        {

        }
    }
}
