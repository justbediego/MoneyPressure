﻿using Core.DataAccess.UnitOfWork;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataAccess.Repository
{
    public class RateRepository : GenericRepository<Rate>
    {
        public RateRepository(MoneyPressureContext dbContext)
            : base(dbContext)
        {

        }
    }
}
