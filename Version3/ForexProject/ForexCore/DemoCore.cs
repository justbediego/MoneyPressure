﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexCore
{
    public class DemoCore
    {
        private string username, password;
        public readonly ReadOnlyCollection<string> Currencies;
        public DemoCore(string username, string password)
        {
            this.username = username;
            this.password = password;
            Currencies = new List<string>().AsReadOnly();
            var currencies = new List<string>() { "USD", "EUR", "GBP", "JPY", "XAU", "AUD", "CHF", "CAD", "NZD", "DKK", "HKD", "XAG", "NOK", "SEK", "SGD", "HUF" };
            Currencies = currencies.AsReadOnly();
        }
        public class DateTimeInstance
        {
            public class RateData
            {
                public double High;
                public double Low;
                public double Open;
                public double Close;
                public double? InstancePressure;
                public double? TotalPressure;
                public double Average
                {
                    get
                    {
                        return (Open + Close) / 2;
                    }
                }
                public RateData(double high, double low, double open, double close)
                {
                    High = high;
                    Low = low;
                    Open = open;
                    Close = close;
                }
            }
            public DateTime DateTime;
            public class Currency
            {
                public double? AveragePressure;
                public RateData[] Rates;
            }
            public Currency[] Currencies;
        }
        public List<DateTimeInstance> GetMatrix(DateTime startDate, DateTime endDate, ChartingService.TimeFrame timeFrame)
        {
            //Authenticate
            AuthenticationService.AuthenticationServiceSoapClient authenticationClient = new AuthenticationService.AuthenticationServiceSoapClient();
            var token = authenticationClient.AuthenticateCredentials(null, "diegocompleted@gmail.com", "mashti");
            if (string.IsNullOrEmpty(token.token))
                throw new Exception("No Token");
            List<DateTimeInstance> data = new List<DateTimeInstance>();
            ChartingService.ChartingServiceSoapClient charClient = new ChartingService.ChartingServiceSoapClient();
            //
            for (int i = 0; i < Currencies.Count; i++)
            {
                for (int j = 0; j < Currencies.Count; j++)
                {
                    if (i == j)
                        continue;
                    var tempData = charClient.GetChartData(null, token.token, Currencies[i] + "/" + Currencies[j], timeFrame, startDate, endDate);
                    if (string.IsNullOrEmpty(tempData.Data))
                        continue;
                    var byDate = tempData.Data.Split('$');
                    foreach (var date in byDate)
                    {
                        var parts = date.Split('\\');
                        DateTime parsedDate = DateTime.Parse(parts[0]);
                        var selectedD = data.FirstOrDefault(d => d.DateTime == parsedDate);
                        if (selectedD == null)
                        {
                            selectedD = new DateTimeInstance()
                            {
                                DateTime = parsedDate,
                                Currencies=new DateTimeInstance.Currency[Currencies.Count],
                            };
                            data.Add(selectedD);
                        }
                        if (selectedD.Currencies[i] == null)
                            selectedD.Currencies[i] = new DateTimeInstance.Currency()
                            {
                                Rates=new DateTimeInstance.RateData[Currencies.Count],
                            };
                        if (selectedD.Currencies[j] == null)
                            selectedD.Currencies[j] = new DateTimeInstance.Currency()
                            {
                                Rates = new DateTimeInstance.RateData[Currencies.Count],
                            };
                        selectedD.Currencies[i].Rates[j] = new DateTimeInstance.RateData(double.Parse(parts[2]), double.Parse(parts[3]), double.Parse(parts[1]), double.Parse(parts[4]));
                        selectedD.Currencies[j].Rates[i] = new DateTimeInstance.RateData(1 / double.Parse(parts[2]), 1 / double.Parse(parts[3]), 1 / double.Parse(parts[1]), 1 / double.Parse(parts[4]));
                    }
                }
            }
            return data.OrderBy(d => d.DateTime).ToList();
        }
        public void CalculatePressure(List<DateTimeInstance> data)
        {
            data= data.OrderBy(d => d.DateTime).ToList();
            for (int d = 0; d < data.Count; d++)
            {
                var instance = data[d];
                if (instance.Currencies == null)
                    continue;
                for(int i = 0; i < Currencies.Count; i++)
                {
                    if (instance.Currencies[i] == null)
                        continue;
                    double averagePressure = 0;
                    int averagePressureCount = 0;
                    for (int j = 0; j < Currencies.Count; j++)
                    {
                        if (instance.Currencies[i].Rates[j] == null)
                            continue;
                        //calculate pressure
                        int count = 0;
                        double p = 0;
                        for(int k = 0; k < Currencies.Count; k++)
                        {
                            if (k == i || k == j)
                                continue;
                            if (instance.Currencies[i].Rates[k] == null || instance.Currencies[k].Rates[j] == null)
                                continue;
                            count++;
                            p += instance.Currencies[i].Rates[k].Average * instance.Currencies[k].Rates[j].Average;
                        }
                        if (count > 0)
                        {
                            p /= count;
                            instance.Currencies[i].Rates[j].InstancePressure = p / instance.Currencies[i].Rates[j].Average - 1;
                            if (d > 0)
                            {
                                var prev = data[d - 1];
                                if (prev.Currencies != null && prev.Currencies[i] != null && prev.Currencies[i].Rates[j] != null && prev.Currencies[i].Rates[j].TotalPressure.HasValue)
                                    instance.Currencies[i].Rates[j].TotalPressure = instance.Currencies[i].Rates[j].InstancePressure + prev.Currencies[i].Rates[j].TotalPressure;
                            }
                            else
                                instance.Currencies[i].Rates[j].TotalPressure = instance.Currencies[i].Rates[j].InstancePressure;
                        }
                        if (instance.Currencies[i].Rates[j].TotalPressure.HasValue)
                        {
                            averagePressure += instance.Currencies[i].Rates[j].TotalPressure.Value;
                            averagePressureCount++;
                        }
                    }
                    //mean currency pressure
                    if (averagePressureCount > 0)
                    {
                        instance.Currencies[i].AveragePressure = averagePressure / averagePressureCount;
                    }
                }
            }
        }
        public void GetBestChoice(List<DateTimeInstance> data, out int currency1, out int currency2, out double rate, out double totalPressure)
        {
            double max = 0;
            currency1 = 0;
            currency2 = 0;
            rate = 0;
            totalPressure = 0;
            var now = data.OrderBy(d => d.DateTime).Last();
            for(int i = 0; i < Currencies.Count; i++)
            {
                for(int j = 0; j < Currencies.Count; j++)
                {
                    if (now.Currencies[i] == null || now.Currencies[i].Rates == null || now.Currencies[i].Rates[j] == null || now.Currencies[i].Rates[j].TotalPressure == null || now.Currencies[i].AveragePressure == null)
                        continue;
                    double push = Math.Abs(now.Currencies[i].Rates[j].TotalPressure.Value / now.Currencies[i].AveragePressure.Value);
                    if (push > max)
                    {
                        max = push;
                        currency1 = i;
                        currency2 = j;
                        rate = now.Currencies[i].Rates[j].Average;
                        totalPressure = now.Currencies[i].Rates[j].TotalPressure.Value;
                    }
                }
            }
        }
    }
}
