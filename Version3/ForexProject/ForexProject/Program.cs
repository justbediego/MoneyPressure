﻿using ForexCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForexProject
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> money = new List<double>();
            DemoCore core = new DemoCore("halizadehir@gmail.com", "hossein");
            core.Currencies.ToList().ForEach(c => money.Add(1));
            //List<DemoCore.DateTimeInstance> data = core.GetMatrix(DateTime.Now.AddDays(-90), DateTime.Now, ForexCore.ChartingService.TimeFrame.THIRTY_MINUTE);
            //File.WriteAllText("d:\\data.txt",JsonConvert.SerializeObject(data));
            var data = JsonConvert.DeserializeObject<List<DemoCore.DateTimeInstance>>(File.ReadAllText("d:\\data.txt"));
            for (int i = 0; i < data.Count - 50; i++)
            {
                var testData = data.Skip(i).Take(200).ToList();
                //current money
                double moneyInUSD = 0;
                var today = testData.Last();
                for (int c = 0; c < core.Currencies.Count; c++)
                {
                    if (c == 0)
                        moneyInUSD += money[0];
                    else
                        moneyInUSD += today.Currencies[c].Rates[0].Average * money[c];
                }
                Console.WriteLine(moneyInUSD);
                //
                core.CalculatePressure(testData);
                int c1, c2;
                double rate, pressure;
                core.GetBestChoice(testData, out c1, out c2, out rate, out pressure);
                if (pressure > 0)
                {
                    //buy
                    double moneyToChange = money[c1] / 2;
                    money[c2] += moneyToChange * rate;
                    money[c1] -= moneyToChange;
                }
                else if (pressure < 0)
                {
                    //sell
                    double moneyToChange = money[c2] / 2;
                    money[c1] += moneyToChange / rate;
                    money[c2] -= moneyToChange;
                }
            }
        }
    }
}
